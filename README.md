Running application
===================

`docker-compose -f docker-compose.yml -f docker-compose.dev.yml up`

Website: `http://192.168.99.100:8080` `user@test.pl:user`

PhpMyAdmin: `http://192.168.99.100:8083` `root:root`

You can run following commands to perform API actions:

`curl -X POST -H "X-AUTH-TOKEN: token" "Content-Type: application/json" http://192.168.99.100:8080/api/user -d '{"name":"test","email":"test@test.pl","plainPassword":{"first":"password","second":"password"}}'`

`curl -X GET -H "X-AUTH-TOKEN: token" "Content-Type: application/json" http://192.168.99.100:8080/api/user/2`

`curl -X PATCH -H "X-AUTH-TOKEN: token" "Content-Type: application/json" http://192.168.99.100:8080/api/user/2 -d '{"name":"test 2"}'`

`curl -X DELETE -H "X-AUTH-TOKEN: token" "Content-Type: application/json" http://192.168.99.100:8080/api/user/2`