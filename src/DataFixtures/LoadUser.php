<?php
namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class LoadUser extends Fixture
{
    /** @var UserPasswordEncoderInterface $encoder */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setName('user');
        $user->setEmail('user@test.pl');
        $password = $this->encoder->encodePassword($user, 'user');
        $user->setPassword($password);
        $user->setToken('token');
        $user->setRole('ROLE_API');

        $manager->persist($user);
        $manager->flush();
    }
}
