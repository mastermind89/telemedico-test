<?php
namespace App\Controller\Api;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class UserController extends AbstractController
{
    /** @var UserRepository $userRepository */
    private $userRepository;

    /** @var UserPasswordEncoderInterface $encoder */
    private $encoder;

    /**
     * UserController constructor.
     * @param UserRepository $userRepository
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserRepository $userRepository, UserPasswordEncoderInterface $encoder)
    {
        $this->userRepository = $userRepository;
        $this->encoder = $encoder;
    }

    /**
     * @Route("/api/user", name="getAllUsers", methods={"GET"})
     * @return JsonResponse
     */
    public function getAllUsers()
    {
        $users = $this->userRepository->findAll();
        return $this->handleResponse([
            'status' => 'ok',
            'message' => 'Users received',
            'data' => $users
        ]);
    }

    /**
     * @Route("/api/user/{id}", name="getUserById", methods={"GET"})
     * @param int $id
     * @return JsonResponse
     */
    public function getUserById(int $id)
    {
        $user = $this->userRepository->find($id);

        if (!$user instanceof User) {
            return $this->handleResponse([
                'status' => 'error',
                'message' => "Invalid data provided: user #{$id} not found"
            ], Response::HTTP_NOT_FOUND);
        }

        return $this->handleResponse($user);
    }

    /**
     * @Route("/api/user", name="createUser", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createUser(Request $request)
    {
        $data = json_decode($request->getContent(),true);

        $user = new User();
        $form = $this->createForm(UserType::class, $user, ['csrf_protection' => false]);
        $form->submit($data, false);

        $password = $this->encoder->encodePassword($user, $user->getPlainPassword());
        $user->setPassword($password);
        $user->setRole('ROLE_USER');

        if (!$form->isValid()) {
            return $this->handleResponse([
                'status' => 'error',
                'message' => $form->getErrors(true)
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $this->userRepository->save($user);

        return $this->handleResponse([
            'status' => 'ok',
            'message' => 'User created',
            'data' => $user
        ]);
    }

    /**
     * @Route("/api/user/{id}", name="updateUser", methods={"PATCH"})
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateUser(Request $request, int $id)
    {
        $user = $this->userRepository->find($id);

        if (!$user instanceof User) {
            return $this->handleResponse([
                'status' => 'error',
                'message' => "Invalid data provided: user #{$id} not found"
            ], Response::HTTP_NOT_FOUND);
        }

        $data = json_decode($request->getContent(),true);
        $originalPassword = $user->getPassword();

        $form = $this->createForm(
            UserType::class,
            $user,
            [
                'csrf_protection' => false,
                'method' => 'PATCH',
                'allow_extra_fields' => false
            ]
        );
        $form->submit($data, false);

        if (!$form->isValid()) {
            return $this->handleResponse([
                'status' => 'error',
                'message' => $form->getErrors()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $plainPassword = $form->get('plainPassword')->getData();
        if (!empty($plainPassword))  {
            $password = $this->encoder->encodePassword($user, $plainPassword);
            $user->setPassword($password);
        } else {
            $user->setPassword($originalPassword);
        }

        $this->userRepository->save($user);

        return $this->handleResponse([
            'status' => 'ok',
            'message' => 'User updated',
            'data' => $user
        ]);
    }

    /**
     * @Route("/api/user/{id}", name="deleteUser", methods={"DELETE"})
     * @param int $id
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteUser(int $id)
    {
        $user = $this->userRepository->find($id);

        if (!$user instanceof User) {
            return $this->handleResponse([
                'status' => 'error',
                'message' => "Invalid data provided: user #{$id} not found"
            ], Response::HTTP_NOT_FOUND);
        }

        $this->userRepository->delete($user);

        return $this->handleResponse([
            'status' => 'ok',
            'message' => 'User deleted'
        ]);
    }

    /**
     * @param $data
     * @return bool|float|int|string
     */
    private function serialize($data)
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $json = $serializer->serialize($data, 'json');
        return $json;
    }

    /**
     * @param $data
     * @param int $status
     * @return JsonResponse
     */
    private function handleResponse($data, $status = Response::HTTP_OK)
    {
        return JsonResponse::fromJsonString($this->serialize($data), $status);
    }
}
