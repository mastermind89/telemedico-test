<?php

namespace Tests\src\Controller\Api;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    const API_URL = 'http://192.168.99.100:8080/api';

    /** @var KernelBrowser $client */
    private $client;

    /** @var int */
    private $userId;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testCreateUser()
    {
        $name = 'test' . uniqid();
        $data = [
            'name' => $name,
            'email' => $name . '@test.pl',
            'plainPassword' => [
                'first' => 'password',
                'second' => 'password'
            ]
        ];

        $this->client->request(
            'POST',
            self::API_URL . '/user',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json', 'HTTP_X_AUTH_TOKEN' => 'token'],
            json_encode($data)
        );

        $response = $this->client->getResponse();
        $responseData = json_decode($response->getContent(), true);
        $this->assertEquals(200, $response->getStatusCode());
        $this->userId = $responseData['data']['id'];
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testUpdateUser()
    {
        $userId = $this->getLastUserId();

        $data = [
            'name' => 'test ' . $userId
        ];

        $this->client->request(
            'PATCH',
            self::API_URL . '/user/' . $userId,
            [],
            [],
            ['CONTENT_TYPE' => 'application/json', 'HTTP_X_AUTH_TOKEN' => 'token'],
            json_encode($data)
        );

        $response = $this->client->getResponse();
        $responseData = json_decode($response->getContent(), true);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('test ' . $userId, $responseData['data']['name']);
    }

    /**
     * @test
     * @throws \Throwable
     */
    public function testDeleteUser()
    {
        $userId = $this->getLastUserId();

        $this->client->request(
            'DELETE',
            self::API_URL . '/user/' . $userId,
            [],
            [],
            ['CONTENT_TYPE' => 'application/json', 'HTTP_X_AUTH_TOKEN' => 'token']
        );

        $response = $this->client->getResponse();
        $responseData = json_decode($response->getContent(), true);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('ok', $responseData['status']);
    }

    private function getLastUserId()
    {
        $this->client->request(
            'GET',
            self::API_URL . '/user',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json', 'HTTP_X_AUTH_TOKEN' => 'token']
        );

        $response = $this->client->getResponse();
        $responseData = json_decode($response->getContent(), true);
        $this->assertEquals(200, $response->getStatusCode());

        return $responseData['data'][count($responseData['data'])-1]['id'];
    }
}
